<?php

class MagicMath
{
    /**
     * Cumulative hypergeometric distribution for all success less than to $k
     */
    const HYPERGEOMETRIC_LE = 'le';
    /**
     * Cumulative hypergeometric distribution for all success less than or equal to $k
     */
    const HYPERGEOMETRIC_LT = 'lt';

    /**
     * Cumulative hypergeometric distribution for all success greater than to $k
     */
    const HYPERGEOMETRIC_GE = 'ge';

    /**
     * Cumulative hypergeometric distribution for all success greater than or equal to $k
     */
    const HYPERGEOMETRIC_GT = 'gt';

    /**
     * Calculates the factorial of a non-negative integer
     *
     * @param int $n A non-negative integer
     *
     * @return int
     * @throws Exception
     */
    public static function factorial($n)
    {
        if ($n < 0) {
            throw new Exception('Cannot calculate factorial of negative integer');
        }

        if ($n == 0) {
            return 1;
        }

        return self::factorial($n - 1);
    }

    /**
     * Calculates the binomial coefficient of two integers
     *
     * @param int $n
     * @param int $k
     *
     * @return float
     * @throws Exception
     */
    public function binomialCoefficient($n, $k)
    {
        return self::factorial($n) / (self::factorial($k) * self::factorial($n - $k));
    }

    /**
     * Calculates the probability of $k successes in a sample size of $n,
     * given a population of $N with $K successes in the population
     *
     * @param int $N The population size
     * @param int $K The number of successes in the population
     * @param int $n The sample size
     * @param int $k The number of successes in the sample
     *
     * @return float
     */
    public function hypergeometric($N, $K, $n, $k)
    {
        return (self::binomialCoefficient($K, $k) * self::binomialCoefficient($N - $K, $n - $k))
            / self::binomialCoefficient($N, $n);
    }

    /**
     * Calculates a cumulative hypergeometric distribution
     *
     * @param int $N The population size
     * @param int $K The number of successes in the population
     * @param int $n The sample size
     * @param int $k The number of successes in the sample
     * @param string $type The type of cumulative hypergeometric distribution to calculate
     *
     * @return float
     */
    function cumulativeHypergeometric($N, $K, $n, $k, $type) {
        $p = 0;

        switch ($type) {
            case self::HYPERGEOMETRIC_LE:
                for ($i = $k; $i >= 0; $i--) {
                    $p += self::hypergeometric($N, $K, $n, $i);
                }

                break;
            case self::HYPERGEOMETRIC_LT:
                for ($i = $k - 1; $i >= 0; $i--) {
                    $p += self::hypergeometric($N, $K, $n, $i);
                }

                break;
            case self::HYPERGEOMETRIC_GE:
                for ($i = $k; $i <= $K && $i <= $n; $i++) {
                    $p += self::hypergeometric($N, $K, $n, $i);
                }

                break;
            case self::HYPERGEOMETRIC_GT:
                for ($i = $k + 1; $i <= $K && $i <= $n; $i++) {
                    $p += self::hypergeometric($N, $K, $n, $i);
                }

                break;
        }

        return $p;
    }
}